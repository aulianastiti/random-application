package com.aulia.myapplication.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.aulia.myapplication.AssetsManager;

/**
 * Created by Aulia Nastiti on 6/8/2017.
 */

public class MainEditText extends EditText {
    public MainEditText(Context context) {
        super(context);
    }

    public MainEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MainEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(AssetsManager.init(getContext()).getMainTypeface());
    }
}
