package com.aulia.myapplication.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.aulia.myapplication.AssetsManager;

/**
 * Created by Aulia Nastiti on 6/8/2017.
 */

public class MainButton extends Button {
    public MainButton(Context context) {
        super(context);
    }

    public MainButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MainButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setTypeface(Typeface tf) {
        AssetsManager am = AssetsManager.init(getContext());
        super.setTypeface(am.getMainTypeface());
    }
}
