package com.aulia.myapplication.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

import com.aulia.myapplication.AssetsManager;

/**
 * Created by Aulia Nastiti on 6/8/2017.
 */

public class MainTextInputLayout extends TextInputLayout {
    public MainTextInputLayout(Context context) {
        super(context);
    }

    public MainTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MainTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(AssetsManager.init(getContext()).getMainTypeface());
    }
}
