package com.aulia.myapplication.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Aulia Nastiti on 6/8/2017.
 */

public class MainTextView extends TextView {
    public MainTextView(Context context) {
        super(context);
    }

    public MainTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MainTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public Typeface getTypeface() {
        Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/tcb.TTF");

        return type;
    }

    @Override
    public void setTypeface(Typeface tf) {
        Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/tcb.TTF");

        super.setTypeface(type);
    }
}
