package com.aulia.myapplication;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aulia Nastiti on 5/17/2017.
 */

public class MyGridAdapter extends BaseAdapter {
    Context context;
    private List<String> texts;
    public MyGridAdapter(Context context){
        this.context = context;
        texts = new ArrayList<>();
        texts.add("hey");
        texts.add("ssup");
        texts.add("hey");
        texts.add("ssup");
        texts.add("hey");
        texts.add("ssup");
        texts.add("hey");
        texts.add("ssup");
        texts.add("hey");
        texts.add("ssup");
    }
    @Override
    public int getCount() {
        return texts.size();
    }

    @Override
    public Object getItem(int position) {
        return texts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = View.inflate(context, R.layout.youtube_grid_item,null);
            TextView text = (TextView)convertView.findViewById(R.id.title);
            text.setText(texts.get(position));
        }

        return convertView;
    }
}
