package com.aulia.myapplication;

import java.util.ArrayList;
import java.util.List;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private Context context;
    private List<String> texts;

    public MyAdapter() {
        texts = new ArrayList<>();
        texts.add("hey");
        texts.add("ssup");
        texts.add("hey");
        texts.add("ssup");
        texts.add("hey");
        texts.add("ssup");
        texts.add("hey");
        texts.add("ssup");
        texts.add("hey");
        texts.add("ssup");
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.youtube_grid_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.youtubeTitle.setText(texts.get(position));
    }

    @Override
    public int getItemCount() {
        return texts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView youtubeTitle;
        ImageView youtubeImage;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setFocusable(true);

            youtubeTitle = (TextView) itemView.findViewById(R.id.title);
        }

    }




}
