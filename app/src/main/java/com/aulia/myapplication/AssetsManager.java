package com.aulia.myapplication;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;


/**
 * Created by Aulia Nastiti on 6/8/2017.
 */

public class AssetsManager {
    private static Context context;
    public static AssetsManager init(Context ctx){
        context = ctx;
        return new AssetsManager();
    }
    public Typeface getMainTypeface(){
        Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/tcb.TTF");
        return type;
    }
}
