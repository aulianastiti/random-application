package com.aulia.myapplication.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.aulia.myapplication.R;
import com.aulia.myapplication.view.MainTextView;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity  {

    FirebaseAuth auth;
    @BindView(com.aulia.myapplication.R.id.sign_up) ImageButton signUp;
    @BindView(R.id.play_vlc) ImageButton playVLC;
    @BindView(R.id.login) ImageButton login;
    @BindView(R.id.map) ImageButton map;
    @BindView(R.id.card_login) CardView card_login;
    @BindView(R.id.card_map) CardView card_map;
    @BindView(R.id.card_play) CardView card_play;
    @BindView(R.id.card_signup) CardView card_signup;
    @BindView(R.id.text_sign_in)
    MainTextView text_signin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_front);
        auth = FirebaseAuth.getInstance();
        ButterKnife.bind(this);
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SignUpActivity.class));
            }
        });
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
            }
        });
        playVLC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, VideoPlayerActivity.class));
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(auth.getCurrentUser()!=null){
                    auth.signOut();
                    auth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
                        @Override
                        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                            checkBtn();
                        }
                    });
                }else{
                    startActivity(new Intent(MainActivity.this, SignInActivity.class));
                }
            }
        });
        checkBtn();
    }
    public void checkBtn(){
        if(auth.getCurrentUser()!=null){
            card_play.setVisibility(View.VISIBLE);
            card_signup.setVisibility(View.GONE);
            card_map.setVisibility(View.VISIBLE);
            text_signin.setText("Sign Out");
        }else{
            card_play.setVisibility(View.GONE);
            card_signup.setVisibility(View.VISIBLE);
            card_map.setVisibility(View.GONE);
            text_signin.setText("Sign In");
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        return super.onTouchEvent(event);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(MainActivity.this, "Permission denied to read your Locatioin", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }
}
