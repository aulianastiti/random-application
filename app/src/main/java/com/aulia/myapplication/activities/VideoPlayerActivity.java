package com.aulia.myapplication.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.aulia.myapplication.R;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;
import org.videolan.libvlc.media.VideoView;

import java.util.ArrayList;

/**
 * Created by Aulia Nastiti on 6/2/2017.
 */

public class VideoPlayerActivity extends AppCompatActivity {

    private LibVLC vlc;
    private MediaPlayer mediaPlayer;
    private VideoView videoView;
    private ProgressBar probar;
    private com.aulia.myapplication.MediaController mc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final String path = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";
        ArrayList<String> options = new ArrayList<>();
        options.add("--aout=opensles");
        options.add("--audio-time-stretch");
        options.add("-vvv");
        options.add("--http-reconnect");
        options.add("--network-caching=" + 6 * 1000);
        vlc = new LibVLC(this,options);
        probar = (ProgressBar) findViewById(R.id.probar);
        videoView = (VideoView)findViewById(R.id.videoview);

        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(mc.isShowing()){
                        mc.stopTimer();
                        return true;
                    }else{
                        mc.startTimer();
                        return true;
                    }
                }
                return false;
            }
        });

        mc = (com.aulia.myapplication.MediaController) findViewById(R.id.mediacontroller);

        mediaPlayer = new MediaPlayer(vlc);

        mediaPlayer.setEventListener(new MediaPlayer.EventListener() {
            @Override
            public void onEvent(MediaPlayer.Event event) {
                switch (event.type){
                    case MediaPlayer.Event.EncounteredError:
                        break;
                    case MediaPlayer.Event.Stopped:
                        probar.setVisibility(View.GONE);
                        break;
                    case MediaPlayer.Event.Opening:
                        mc.play();
                        break;
                    case MediaPlayer.Event.Vout:
                        break;
                    case MediaPlayer.Event.EndReached:
                        mc.getBtnPlay().setImageResource(android.R.drawable.ic_media_play);
                        final Media media = new Media(vlc, Uri.parse(path));
                        media.setHWDecoderEnabled(true,true);
                        mediaPlayer.setMedia(media);
                        break;

                }
            }
        });
        final Media media = new Media(vlc,Uri.parse(path));
        media.setHWDecoderEnabled(true,true);
        mediaPlayer.setMedia(media);
        mediaPlayer.play();

        IVLCVout ivlcVout = mediaPlayer.getVLCVout();
        ivlcVout.setVideoView(videoView);
        ivlcVout.addCallback(new IVLCVout.Callback() {
            @Override
            public void onNewLayout(IVLCVout vlcVout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {

            }

            @Override
            public void onSurfacesCreated(IVLCVout vlcVout) {

            }

            @Override
            public void onSurfacesDestroyed(IVLCVout vlcVout) {
            }

            @Override
            public void onHardwareAccelerationError(IVLCVout vlcVout) {

            }

        });

        ivlcVout.attachViews();


        this.mc = (com.aulia.myapplication.MediaController) findViewById(R.id.mediacontroller);
        this.mc.setMediaPlayer(mediaPlayer);
//        MyAdapter adapter = new MyAdapter();
//        RecyclerView rc = (RecyclerView)findViewById(R.id.rc);
//        GridLayoutManager layout = new GridLayoutManager(this, 3);
//        rc.setLayoutManager(layout);
//        rc.setAdapter(adapter);
//
//        MyGridAdapter adapter = new MyGridAdapter(this);
//        GridView grid = (GridView)findViewById(R.id.rc);
//        grid.setAdapter(adapter);
    }

    @Override
    protected void onPause() {
        mediaPlayer.stop();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mediaPlayer !=null )mediaPlayer.play();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        return super.onTouchEvent(event);
    }


}
