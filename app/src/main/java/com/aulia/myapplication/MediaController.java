package com.aulia.myapplication;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.videolan.libvlc.MediaPlayer;

/**
 * Created by Aulia Nastiti on 5/29/2017.
 */

public class MediaController extends LinearLayout implements View.OnClickListener{
    private ImageButton btn_play;
    private ImageButton btn_next;
    private ImageButton btn_previous;
    private TextView currentDuration;
    private TextView duration;
    private SeekBar seekBar;

    private Handler mHandler;

    private MediaPlayer mediaPlayer;

    private boolean isMenuShowing = false;


    private MyCountDownTimer timer;
    public MediaController(Context context) {
        super(context);
        init();

    }

    public MediaController(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MediaController(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        LayoutInflater.from(getContext()).inflate(R.layout.media_controller, this);
        mHandler = new Handler();

        long startTime = 5 * 1000;
        long interval = 1 * 1000;
        timer = new MyCountDownTimer(startTime,interval);
        timer.start();

        btn_next = (ImageButton) findViewById(R.id.youtube_video_next);
        btn_play = (ImageButton) findViewById(R.id.youtube_video_play);
        btn_previous = (ImageButton) findViewById(R.id.youtube_video_prev);

        seekBar = (SeekBar) findViewById(R.id.youtube_video_seekbar);
        currentDuration = (TextView) findViewById(R.id.youtube_video_play_time);
        duration = (TextView) findViewById(R.id.youtube_video_play_time2);
    }

    public ImageButton getBtnNext(){
        return  btn_next;
    }
    public ImageButton getBtnPlay(){
        return btn_play;
    }

    public ImageButton getBtn_previous(){
        return btn_previous;
    }

    public SeekBar getSeekBar(){return seekBar;}
    public TextView getDurationView(){return duration;}

    public void setMediaPlayer(final MediaPlayer mp){
        this.mediaPlayer = mp;
        btn_previous.setOnClickListener(this);
        btn_next.setOnClickListener(this);
        btn_play.setOnClickListener(this);

        duration.setText(formatTime((int)mediaPlayer.getTime()));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    Log.e("time", String.valueOf(progress));
                    int pos = (progress * (int)mediaPlayer.getLength())/100;
                    mediaPlayer.setTime(pos);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mHandler.postDelayed(runnable, 100);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.youtube_video_next:

                seek(3000);
                break;
            case R.id.youtube_video_play:
                if(mediaPlayer.isPlaying()){
                    pause();
                    break;
                }
                play();
                break;
            case R.id.youtube_video_prev:
                seek(-3000);
                break;
        }
    }

    public String formatTime(int millis) {
        int seconds = millis / 1000;
        int minutes = seconds / 60;
        int hours = minutes / 60;

        return (hours == 0 ? "" : hours + ":") + String.format("%02d:%02d", minutes % 60, seconds % 60);
    }

    private void displayCurrentTime() {
        if (null == mediaPlayer) return;

        if (mediaPlayer.getLength() > 0) {
            int progress = (int)(mediaPlayer.getTime()*100/ mediaPlayer.getLength());
            seekBar.setProgress(progress);
            String formattedTime = formatTime((int)mediaPlayer.getTime());
            currentDuration.setText(formattedTime);
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            displayCurrentTime();
            mHandler.postDelayed(this, 100);
        }
    };

    public void pause(){
        mediaPlayer.pause();
        btn_play.setImageResource(android.R.drawable.ic_media_play);
    }

    public void play(){
        mediaPlayer.play();
        btn_play.setImageResource(android.R.drawable.ic_media_pause);
    }

    public void seek(int delta) {
        // unseekable stream
        if (mediaPlayer.getLength() <= 0)
            return;

        long position = mediaPlayer.getTime() + delta;
        if (position < 0)
            position = 0;
        mediaPlayer.setTime(position);
    }

    private class MyCountDownTimer extends CountDownTimer {

        MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            hide();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (!isMenuShowing)
                show();
        }
    }

    private void hide() {
//            Animation slide_out = AnimationUtils.loadAnimation(getContext(), R.anim.slide_down);
//            startAnimation(slide_out);
            setVisibility(View.GONE);
            isMenuShowing = false;
    }

    private void show() {
//            Animation slide_in = AnimationUtils.loadAnimation(getContext(), R.anim.draw_from_bottom);
            setVisibility(View.VISIBLE);
//            startAnimation(slide_in);
            isMenuShowing = true;
    }

    public boolean isShowing(){
        return isMenuShowing;
    }

    public void startTimer(){
        timer.cancel();
        timer.start();
    }

    public void stopTimer(){
        timer.cancel();
        timer.onFinish();
    }
}
